#include <iostream>

using namespace std;
//programa imprime una matriz 5x5, y la voltea 90º,180º y 270º
int main()
{int matriz[5][5];
    for(int i=0;i<5;i++){//ciclo que va poniendo los numeros ingresados por el usuario en las posiciones de la matriz
        for(int z=0;z<5;z++){
            cout<<"ingrese numeros, el numero que se ingresa se guarda en la posicion: ["<<i<<"]  ["<<z<<"]"<<endl;
            cin>>matriz[i][z];
        }
    }
    //ciclos que van rotando e imprimiendo las matrices segun lo pedido por el ejercicio
    cout<<"matriz original"<<endl;
    for(int i=0;i<5;i++){
        for(int z=0;z<5;z++){
            cout<<matriz[i][z]<<"|";
        }
        cout<<endl;
    }
    cout<<"matriz rotada 90 grados"<<endl;
    for(int i=0;i<5;i++){
        for(int z=5;z>0;z--){
            cout<<matriz[z-1][i]<<"|";
        }
        cout<<endl;
    }

       cout<<endl<<"matriz rotada 180 grados"<<endl;
    for(int i=5;i>0;i--){
       for(int z=5;z>0;z--){
        cout<<matriz[i-1][z-1]<<"|";
       }
       cout<<endl;
    }
    cout<<endl<<"matriz rotada 270 grados"<<endl;
 for(int i=5;i>0;i--){
    for(int z=0;z<5;z++){
     cout<<matriz[z][i-1]<<"|";
    }
    cout<<endl;
 }
    return 0;
}
